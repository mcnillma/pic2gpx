#!/usr/bin/env python
# -*- coding: utf-8 -*-
# VERSION 1.0
#
#  This Script creates GPX-Track File from the GPS tags in jpg files.
#
#  Copyright (c) 2013 Marcus Nill All rights reserved
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#  This script uses code from exif.py.
#  See exif.py for further infos about the authors und license.
#
#  Usage:
#  Call the script from the folder containing the pictures.
#  python /path/to/script/pic2gpx.py in /home/marcus/Pictures/hiking012
#  The output will be placed in the pictures folder with the same name
#  as the picture folder hiking012.gpx.

import sys
import EXIF
import os
import shlex
import datetime


#####Note
#Convert the GPS-data: a = degree, b = minutes, c = seconds --> a + (b+(c/60))/60
#####TODO
#Validate the output file on "http://www.topografix.com/gpx_validation.asp"
#Using an existing library for converting the GPS-data

path = os.getcwd()
global autorname
try:
    autorname = os.environ["USER"]
except:
    autorname = "--CREATOR--"
global trackname
trackname = os.path.split(path)[1]
global creationdate
creationdate = datetime.datetime.utcnow()
creationdate = str(creationdate).replace(" ", "T")

#GPX Skeleton
templ = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gpx version="1.1" creator="pic2gpx.py" xmlns="http://www.topografix.com/GPX/1/1">
    <metadata>
        <name>--trackname--</name>
        <author>
            <name>--autorname--</name>
        </author>
        <time>--creationdate--</time>
    </metadata>
    <trk>
        <name>--trackname--</name>
        <desc></desc>
        <trkseg>
--waypoints--
        </trkseg>
    </trk>
</gpx>"""

def get_files(path):
    rawlist = os.listdir(path)
    filelist = []
    for i in rawlist:
        if i.endswith(".JPG") or i.endswith(".jpg"):
            filelist.append(os.path.join(path,i))
    filelist.sort()
    return filelist

def read_tags(mediafile):
    gpsinfos = {"GPS GPSLatitude":0, "GPS GPSLatitudeRef":"", "GPS GPSLongitude":0, "GPS GPSLongitudeRef":""}
    dat = open(mediafile,"rb")
    inhalt = EXIF.process_file(dat)
    k = inhalt.keys()
    k.sort()
    for i in k:
        if i in gpsinfos:
            try:
                gpsinfos[i] = inhalt[i].printable
            except:
                print 'error', i, '"', inhalt[i], '"'
    return gpsinfos
    
def get_tags(path):
    filelist = get_files(path)
    waypoints = []
    for pic in filelist:
        print pic
        gpsinfos = read_tags(pic)
        if gpsinfos["GPS GPSLatitude"] == 0:
            print "No GPS values found!"
            continue
        else:
            #Convert the raw tags to usable values
            dms_lat = gpsinfos["GPS GPSLatitude"]
            dms_lat = shlex.split(dms_lat.replace("[","").replace("]","").replace(",",""))
            dms_lat_sec = dms_lat[2].split("/")
            lat_list = [int(dms_lat[0]), int(dms_lat[1]), float(dms_lat_sec[0])/ float(dms_lat_sec[1])]
            dms_lon = gpsinfos["GPS GPSLongitude"]
            dms_lon = shlex.split(dms_lon.replace("[","").replace("]","").replace(",",""))
            dms_lon_sec = dms_lon[2].split("/")
            lon_list = [int(dms_lon[0]), int(dms_lon[1]), float(dms_lon_sec[0])/ float(dms_lon_sec[1])]
            #Calculate the GPS-data from deg,min,sec to decimal
            lattag = lat_list[0] + (lat_list[1]+(lat_list[2]/60))/60
            lontag = lon_list[0] + (lon_list[1]+(lon_list[2]/60))/60
            waypoints.append('{"lat":' + str(lattag) + ',' '"lon":' + str(lontag) + '}')
    return waypoints

def build_track(templ, path):
    waypointlist = get_tags(path)
    waypoints = ""
    tester = ""
    for i in waypointlist:
        i =  eval(i)
        waypoint = '            <trkpt lat=' + '"' + str(i["lat"]) + '"' + ' lon=' + '"' + str(i["lon"]) + '"' + '/> \n'
        if waypoint == tester:
            continue
        else:
            waypoints = waypoints + waypoint
        tester = waypoint
    templ = templ.replace("--waypoints--", waypoints)
    templ = templ.replace("--autorname--", autorname)
    templ = templ.replace("--trackname--", trackname)
    templ = templ.replace("--creationdate--", creationdate)
    return templ

def save_track(path, templ):
    track = build_track(templ, path)
    trackfile = open(os.path.join(path,trackname+".gpx"), "w")
    trackfile.write(track)
    trackfile.close()

save_track(path, templ)


