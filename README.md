#PIC2GPX
Creates GPX tracks from geotagged pictures.
Currently only for jpg.
PIC2GPX is using exif.py from [https://github.com/ianare/exif-py][1].

##USAGE:
Go to your pictures folder.
Call `python /path/to/pic2gpx.py`
The script will place the trackfile into the pictures folder and it will be named as the folders name.

[1]: https://github.com/ianare/exif-py
